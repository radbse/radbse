using System.Collections.Generic;
using System.Net.Mail;
using System.Threading.Tasks;

namespace RADBSE.Services.Email
{
    public interface IEmailService
    {
        Task Send(string from, IEnumerable<string> to, string subject, string body, IEnumerable<string> bcc = null, IEnumerable<Attachment> attachments = null);
    }
}