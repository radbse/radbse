using FluentValidation;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;
using RADBSE.Domain;

namespace Microsoft.AspNetCore.Builder
{
    public static class DomainExtensions
    {
        public static void AddAppDomain(this IServiceCollection services)
        {
            services.Scan(scan => scan.FromCallingAssembly()
                .AddClasses(c => c.AssignableTo(typeof(IValidator<>))).AsImplementedInterfaces().WithTransientLifetime()

                //Anything taking DbContext needs to be scoped lifetime (once per request)                
                .AddClasses(c => c.AssignableTo(typeof(AsyncDbRequestHandler<>))).AsImplementedInterfaces().WithScopedLifetime()
                .AddClasses(c => c.AssignableTo(typeof(AsyncDbRequestHandler<,>))).AsImplementedInterfaces().WithScopedLifetime()
            );

            //Order of behaviors matters here...
            services.AddScoped(typeof(IPipelineBehavior<,>), typeof(ValidationBehavior<,>));
            services.AddMediatR(typeof(ValidationBehavior<,>).GetTypeInfo().Assembly);
        }
    }
}