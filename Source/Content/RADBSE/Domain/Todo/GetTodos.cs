using System;
using System.Threading.Tasks;
using MediatR;
using Mapster;
using RADBSE.Data;
using FluentValidation;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace RADBSE.Domain
{
    public class GetTodos
    {
        public class Query : IRequest<IEnumerable<Model>>
        {

        }

        public class Model
        {
            public Guid Id { get; set; }
            public string Name { get; set; }
            public bool IsCompleted { get; set; }
        }

        public class Handler : AsyncDbRequestHandler<Query, IEnumerable<Model>>
        {
            public Handler(ApplicationDbContext context, IHttpContextAccessor httpContextAccessor) : base(context, httpContextAccessor)
            {

            }

            protected async override Task<IEnumerable<Model>> Handle(Query message)
            {
                return (await DbContext.Todos.Where(t => t.UserId == UserId).ToListAsync().ConfigureAwait(false)).Adapt<IEnumerable<Model>>();
            }
        }
    }
}