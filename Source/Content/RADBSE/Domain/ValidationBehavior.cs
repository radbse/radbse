using System;
using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using MediatR;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.DependencyInjection;

namespace RADBSE.Domain
{
    public class ValidationBehavior<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse>
    {
        private readonly ILogger _logger;
        private readonly IServiceProvider _container;

        public ValidationBehavior(IServiceProvider container, ILoggerFactory loggerFactory)
        {
            _container = container;
            _logger = loggerFactory.CreateLogger<ValidationBehavior<TRequest, TResponse>>();
        }

        public async Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate<TResponse> next)
        {
            using (_logger.BeginScope("Validating Command {Command}", typeof(TRequest).FullName))
            {
                var validator = _container.GetService<IValidator<TRequest>>();

                _logger.LogDebug("Starting Validation");

                if (validator == null)
                    _logger.LogDebug("No Validator, Nothing To Do");
                else
                    await validator.ValidateAndThrowAsync(request).ConfigureAwait(false);

                using (_logger.BeginScope("Executing {Command} Handler", typeof(TRequest).FullName))
                {
                    return await next().ConfigureAwait(false);
                }
            }
        }
    }
}