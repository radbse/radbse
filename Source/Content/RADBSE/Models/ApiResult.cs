using System;
using System.Collections.Generic;
using System.Linq;
using FluentValidation;

namespace RADBSE.Models
{
    public class ApiResult<T>
    {
        public static ApiResult<T> WithResult(T result)
        {
            return new ApiResult<T> { Result = result };
        }

        public bool Success => Errors?.Any() != true;

        public IEnumerable<string> Errors { get; set; }

        public T Result { get; set; }

        public static ApiResult<T> WithError(Exception ex)
        {
            if (ex is ValidationException validationException)
            {
                return new ApiResult<T> { Errors = validationException.Errors.Select(e => e.ErrorMessage) };
            }
            return new ApiResult<T> { Errors = new List<string>(new[] { ex.Message }) };
        }

        public static ApiResult<T> WithErrors(IEnumerable<string> errors)
        {
            return new ApiResult<T> { Errors = errors };
        }
    }

    public class ApiResult
    {

        public bool Success => Errors?.Any() != true;

        public IEnumerable<string> Errors { get; set; }

        public static ApiResult WithSuccess()
        {
            return new ApiResult();
        }

        public static ApiResult WithError(Exception ex)
        {
            if (ex is ValidationException validationException)
            {
                return new ApiResult { Errors = validationException.Errors.Select(e => e.ErrorMessage) };
            }

            return new ApiResult { Errors = new List<string>(new[] { ex.Message }) };
        }

        public static ApiResult WithErrors(IEnumerable<string> errors)
        {
            return new ApiResult { Errors = errors };
        }
    }
}