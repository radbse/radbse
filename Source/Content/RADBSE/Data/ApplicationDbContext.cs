﻿using IdentityServer4.EntityFramework.Options;
using Microsoft.AspNetCore.ApiAuthorization.IdentityServer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
#if (IsNpgSql)
using Npgsql.NameTranslation;
#endif
namespace RADBSE.Data
{
    public class ApplicationDbContext : ApiAuthorizationDbContext<ApplicationUser>
    {
        public ApplicationDbContext(
            DbContextOptions options,
            IOptions<OperationalStoreOptions> operationalStoreOptions) : base(options, operationalStoreOptions)
        {
        }

#if (IsSampleApp)
        public DbSet<Todo> Todos { get; set; }
#endif

#if (IsNpgSql)
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            ApplySnakeCaseNames(modelBuilder);
        }
        
        public void ApplySnakeCaseNames(ModelBuilder modelBuilder)
        {
            static string toSnakeCase(string name) => NpgsqlSnakeCaseNameTranslator.ConvertToSnakeCase(name);

            foreach (var entity in modelBuilder.Model.GetEntityTypes())
            {
                entity.SetTableName(toSnakeCase(entity.GetTableName()));

                foreach (var key in entity.GetKeys())
                {
                    key.SetName(toSnakeCase(key.GetName()));
                }

                foreach (var key in entity.GetForeignKeys())
                {
                    key.SetConstraintName(toSnakeCase(key.GetConstraintName()));
                }

                foreach (var idx in entity.GetIndexes())
                {
                    idx.SetDatabaseName(toSnakeCase(idx.GetDatabaseName()));
                }
            }
        }
#endif
    }
}
