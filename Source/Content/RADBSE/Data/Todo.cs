using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Microsoft.AspNetCore.Identity;

namespace RADBSE.Data
{
    public class Todo
    {
        public Guid Id { get; set; }
        public string UserId { get; set; }
        public string Name { get; set; }
        public bool IsCompleted { get; set; }

        public ApplicationUser User { get; set; }
    }
}