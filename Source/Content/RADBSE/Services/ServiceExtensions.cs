using FluentValidation;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;
using RADBSE.Domain;
using RADBSE.Services.Email;

namespace Microsoft.AspNetCore.Builder
{
    public static class ServiceExtensions
    {
        public static void AddAppServices(this IServiceCollection services)
        {
            services.AddTransient<IEmailService, SmtpEmailService>();
        }

        public static void AddDevelopmentAppServices(this IServiceCollection services)
        {
            services.AddTransient<IEmailService, DebugEmailService>();
        }
    }
}