using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace RADBSE.Services.Email
{
    public class DebugEmailService : IEmailService
    {
        private readonly ILogger<DebugEmailService> _logger;
        public DebugEmailService(ILoggerFactory loggerFactory)
        {
            _logger = loggerFactory.CreateLogger<DebugEmailService>();
        }
        public async Task Send(string from, IEnumerable<string> to, string subject, string body, IEnumerable<string> bcc = null, IEnumerable<Attachment> attachments = null)
        {
            _logger.LogDebug(@"From: {From}
            To: {To}
            Bcc: {Bcc}
            Subject: {Subject}
            Attachments: {Attachments}
            -------------------------------------------
            {Body}", from, to != null ? string.Join(", ", to) : to, bcc != null ? string.Join(", ", bcc) : bcc, subject, attachments != null ? string.Join(", ", attachments.Select(a => a.Name)) : null, body);
        }
    }
}