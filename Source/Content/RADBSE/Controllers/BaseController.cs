using System;
using System.Threading;
using System.Threading.Tasks;
using RADBSE.Models;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace RADBSE.Controllers
{
    public class BaseController : Controller
    {
        private readonly IMediator _mediator;
        public BaseController(IMediator mediator, ILogger logger)
        {
            _mediator = mediator;
            Logger = logger;
        }

        public ILogger Logger { get; set; }

        public async Task<ApiResult> Publish<TNotification>(TNotification notification, CancellationToken cancellationToken = default) where TNotification : INotification
        {
            try
            {
                await _mediator.Publish(notification, cancellationToken).ConfigureAwait(false);
                return ApiResult.WithSuccess();
            }
            catch (Exception ex)
            {
                return ApiResult.WithError(ex);
            }
        }

        public async Task<ApiResult<TResponse>> Send<TResponse>(IRequest<TResponse> request, CancellationToken cancellationToken = default)
        {
            try
            {
                var result = await _mediator.Send(request, cancellationToken).ConfigureAwait(false);
                return ApiResult<TResponse>.WithResult(result);
            }
            catch (Exception ex)
            {
                return ApiResult<TResponse>.WithError(ex);
            }
        }
        public async Task<ApiResult<T>> Send<T>(IRequest request, T returnValue, CancellationToken cancellationToken = default)
        {
            var result = await Send(request, cancellationToken).ConfigureAwait(false);
            if (result.Success) return ApiResult<T>.WithResult(returnValue);
            return ApiResult<T>.WithErrors(result.Errors);
        }
        public async Task<ApiResult<T>> Send<T>(IRequest request, Func<Task<ApiResult<T>>> returnValue, CancellationToken cancellationToken = default(CancellationToken))
        {
            try
            {
                var result = await Send(request, cancellationToken).ConfigureAwait(false);
                if (result.Success) return await returnValue().ConfigureAwait(false);
                return ApiResult<T>.WithErrors(result.Errors);
            }
            catch (System.Exception ex)
            {
                return ApiResult<T>.WithError(ex);
            }

        }
        public async Task<ApiResult> Send(IRequest request, CancellationToken cancellationToken = default)
        {
            try
            {
                await _mediator.Send(request, cancellationToken).ConfigureAwait(false);
                return ApiResult.WithSuccess();
            }
            catch (Exception ex)
            {
                return ApiResult.WithError(ex);
            }
        }
    }
}