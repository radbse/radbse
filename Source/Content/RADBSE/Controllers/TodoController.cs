using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using System.Threading.Tasks;
using RADBSE.Domain;
using System;

namespace RADBSE.Controllers
{
    [Authorize]
    [Route("/api/[controller]")]
    public class TodoController : BaseController
    {
        public TodoController(IMediator mediator, ILoggerFactory loggerFactory) : base(mediator, loggerFactory.CreateLogger<TodoController>())
        {
        }

        [HttpGet]
        public async Task<IActionResult> GetAll() => Ok(await Send(new GetTodos.Query()).ConfigureAwait(false));

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] CreateTodo.Command command) => Ok(await Send(command, command.Id).ConfigureAwait(false));

        [HttpPut]
        [Route("{id}")]
        public async Task<IActionResult> Update([FromRoute] Guid id, [FromBody] UpdateTodo.Command command) => Ok(await Send(command, async () => await Send(new GetTodo.Query { Id = id }).ConfigureAwait(false)).ConfigureAwait(false));
    }
}