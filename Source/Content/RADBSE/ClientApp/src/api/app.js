import { atom, useRecoilState, useSetRecoilState } from 'recoil'

export const alertState = atom({
    key: 'alertState',
    default: { content: null, timeout: 0 },
})

export const useSetAlert = () => {
    const setAlert = useSetRecoilState(alertState)
    return (alert) => setAlert(alert)
}
