import { useEffect, useState } from 'react'
import { useHistory } from 'react-router-dom'
import authService from './authorization/AuthorizeService'

export const useSession = () => {
    const [session, setSession] = useState({ isAuthenticated: false, user: null })
    useEffect(() => {
        const getSession = async () => {
            const user = await authService.getUser()
            setSession({ isAuthenticated: user !== null, user })
        }
        getSession()
    }, [])
    return session
}

export const useLogout = () => {
    return (path) => {
        authService.signOut({ returnUrl: path })
    }
}

export const useLogin = () => {
    return (path) => {
        authService.signIn({ returnUrl: path })
    }
}

export const useRegister = () => {
    const history = useHistory()
    return () => {
        history.push('/authentication/register')
    }
}
