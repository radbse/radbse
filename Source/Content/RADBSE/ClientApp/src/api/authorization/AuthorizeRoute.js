import React from 'react'
import { Component } from 'react'
import { Route, Redirect } from 'react-router-dom'
import { ApplicationPaths, QueryParameterNames } from './ApiAuthorizationConstants'
import authService from './AuthorizeService'

export default class AuthorizeRoute extends Component {
    constructor(props) {
        super(props)

        this.state = {
            ready: false,
            authenticated: false,
            roles: [],
            permissions: [],
        }
    }

    componentDidMount() {
        this._subscription = authService.subscribe(() => this.authenticationChanged())
        this.populateAuthenticationState()
    }

    componentWillUnmount() {
        authService.unsubscribe(this._subscription)
    }

    render() {
        const { ready, authenticated, roles, permissions } = this.state

        const { roles: requiredRoles, permissions: requiredPermissions } = this.props

        const satisfiesRoles = requiredRoles ? roles.some((r) => requiredRoles.indexOf(r) > -1) : true
        const satisfiesPermissions = requiredPermissions
            ? permissions.some((r) => requiredPermissions.indexOf(r) > -1)
            : true

        const isValid = authenticated && satisfiesRoles && satisfiesPermissions

        var link = document.createElement('a')
        link.href = this.props.path
        const returnUrl = `${link.protocol}//${link.host}${link.pathname}${link.search}${link.hash}`
        const redirectUrl =
            authenticated && !isValid
                ? '/home'
                : `${ApplicationPaths.Login}?${QueryParameterNames.ReturnUrl}=${encodeURIComponent(returnUrl)}`
        if (!ready) {
            return <div></div>
        } else {
            const { component: Component, ...rest } = this.props
            return (
                <Route
                    {...rest}
                    render={(props) => {
                        if (isValid) {
                            return <Component {...props} />
                        } else {
                            return <Redirect to={redirectUrl} />
                        }
                    }}
                />
            )
        }
    }

    async populateAuthenticationState() {
        const profile = await authService.getUser()
        const authenticated = !!profile
        const roles = !authenticated ? [] : Array.isArray(profile.role) ? [...profile.role] : [profile.role]
        const permissions = !authenticated
            ? []
            : Array.isArray(profile.permission)
            ? [...profile.permission]
            : [profile.permission]
        this.setState({ ready: true, authenticated, roles, permissions })
    }

    async authenticationChanged() {
        this.setState({ ready: false, authenticated: false, roles: [], permissions: [] })
        await this.populateAuthenticationState()
    }
}
