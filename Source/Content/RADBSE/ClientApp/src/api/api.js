import { useState, useCallback, useEffect } from 'react'
import { useRecoilState } from 'recoil'
import authService from './authorization/AuthorizeService'

export class ApiError extends Error {
    constructor(message, data) {
        super(message)
        this.error = data

        if (Error.captureStackTrace) Error.captureStackTrace(this, ApiError)
    }
}

const getToken = async () => {
    const expiresAt = await authService.getExpiration()
    if (expiresAt && expiresAt <= Date.now() / 1000) {
        await authService.signOut({})
    }
    return await authService.getAccessToken()
}

export const getJson = async (url) => {
    try {
        const token = await getToken()
        const response = await fetch(url, {
            headers: !token ? {} : { Authorization: `Bearer ${token}` },
            cache: 'no-store',
        })
        const json = await response.json()

        if (json.success) return json.result

        if (json.errors) throw new ApiError('server returned an error response', json.errors)
    } catch (error) {}
}

export const postJson = async (url, data) => {
    const token = await getToken()
    const headers = { Accept: 'application/json', 'Content-Type': 'application/json' }

    const response = await fetch(url, {
        headers: !token ? { ...headers } : { ...headers, Authorization: `Bearer ${token}` },
        method: 'POST',
        body: JSON.stringify(data),
    })

    const json = await response.json()

    if (json.success) return json.result

    if (json.errors) throw new ApiError('server returned an error response', json.errors)
}

export const putJson = async (url, data) => {
    const token = await getToken()
    const headers = { Accept: 'application/json', 'Content-Type': 'application/json' }

    const response = await fetch(url, {
        headers: !token ? { ...headers } : { ...headers, Authorization: `Bearer ${token}` },
        method: 'PUT',
        body: JSON.stringify(data),
    })

    const json = await response.json()

    if (json.success) return json.result

    if (json.errors) throw new ApiError('server returned an error response', json.errors)
}

export const deleteJson = async (url, data) => {
    const token = await getToken()
    const headers = { Accept: 'application/json', 'Content-Type': 'application/json' }

    const response = await fetch(url, {
        headers: !token ? { ...headers } : { ...headers, Authorization: `Bearer ${token}` },
        method: 'DELETE',
        body: JSON.stringify(data),
    })

    const json = await response.json()

    if (json.success) return json.result

    if (json.errors) throw new ApiError('server returned an error response', json.errors)
}

export const postFormData = async (url, data) => {
    const token = await getToken()
    const headers = { Accept: 'application/json' }

    const response = await fetch(url, {
        headers: !token ? { ...headers } : { ...headers, Authorization: `Bearer ${token}` },
        method: 'POST',
        body: data,
    })

    const json = await response.json()

    if (json.success) return json.result

    if (json.errors) throw new ApiError('server returned an error response', json.errors)
}

export const listModifier = (cb) => (list, item) => cb(list, item)

export const useLoadableState = (apiCallback, recoilState, modifier) => {
    const [loading, setLoading] = useState(false)
    const [state, setState] = useRecoilState(recoilState)
    const loadState = useCallback(
        async (data) => {
            try {
                setLoading(true)
                const stateResponse = await apiCallback(data)
                if (!modifier) setState(stateResponse)
                else setState((s) => modifier(s, stateResponse))
                setLoading(false)
            } catch (error) {
                setLoading(false)
                throw error
            }
        },
        [setState]
    )

    return [loading, state, loadState]
}

export const useMountableState = (apiCallback, recoilState, deps) => {
    const [loading, state, loadState] = useLoadableState(apiCallback, recoilState)

    useEffect(() => {
        loadState()
    }, deps)

    return [loading, state, loadState]
}

export const useCommandState = (apiCallback, recoilState, modifier) => {
    const [loading, state, execute] = useLoadableState(apiCallback, recoilState, modifier)
    return [
        loading,
        async (data) => {
            await execute(data)
        },
    ]
}
