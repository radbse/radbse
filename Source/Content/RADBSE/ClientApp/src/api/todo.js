import { atom } from 'recoil'
import { getJson, postJson, putJson, listModifier, useMountableState, useCommandState, deleteJson } from './api'

export const todoListState = atom({
    key: 'todoListState',
    default: [],
})

const addListItem = listModifier((list, item) => [...list, item])
const updateListItem = listModifier((list, item) => [...list.map((li) => (li.id === item.id ? item : li))])
const deleteListItem = listModifier((list, item) => [...list.filter((li) => li.id !== item.id)])

const getTodos = async () => getJson('api/todo')
const addTodo = async (todo) => ({ ...todo, id: await postJson('api/todo', todo) })
const updateTodo = async (todo) => await putJson(`api/todo/${todo.id}`, todo)
const deleteTodo = async (todo) => await deleteJson(`api/todo/${todo.id}`)

export const useTodos = () => useMountableState(getTodos, todoListState, [])
export const useAddTodo = () => useCommandState(addTodo, todoListState, addListItem)
export const useUpdateTodo = () => useCommandState(updateTodo, todoListState, updateListItem)
export const useDeleteTodo = () => useCommandState(deleteTodo, todoListState, deleteListItem)
