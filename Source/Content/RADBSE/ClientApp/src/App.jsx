import React, { Suspense } from 'react'
import { RecoilRoot } from 'recoil'
import AuthorizeRoute from './api/authorization/AuthorizeRoute'
import Layout from './containers/Layout'
import ApiAuthorizationRoutes from './api/authorization/ApiAuthorizationRoutes'
import { ApplicationPaths } from './api/authorization/ApiAuthorizationConstants'
import { Route } from 'react-router'
import Loading from './components/Loading'
function App() {
    return (
        <RecoilRoot>
            <Route path={ApplicationPaths.ApiAuthorizationPrefix} component={ApiAuthorizationRoutes} />
            <Suspense fallback={<Loading content="Please wait..." />}>
                <Route path="/" component={Layout} />
            </Suspense>
        </RecoilRoot>
    )
}

export default App
