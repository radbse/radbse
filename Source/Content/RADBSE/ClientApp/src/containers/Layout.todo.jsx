import React from 'react'
import { Route } from 'react-router-dom'
import HomepageHeading from '../components/HomepageHeading'
import ResponsiveNavbar from '../components/ResponsiveNavbar'
import HomePage from './HomePage'
import Alert from '../components/Alert'
import TodoPage from './TodoPage'
import AuthorizeRoute from '../api/authorization/AuthorizeRoute'

const Layout = () => {
    return (
        <div>
            <Alert />
            <ResponsiveNavbar>
                <Route exact path="/" component={HomepageHeading} />
            </ResponsiveNavbar>
            <Route exact path="/" component={HomePage} />
            <AuthorizeRoute path="/todo" component={TodoPage} />
        </div>
    )
}

export default Layout
