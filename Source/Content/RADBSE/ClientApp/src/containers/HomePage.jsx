import React from 'react'
import { useDocumentTitle } from '../hooks'
import { useSetAlert } from '../api/app'

import { Button } from 'semantic-ui-react'

const HomePage = () => {
    var setAlert = useSetAlert()
    useDocumentTitle('RADBSE | HOME')

    const handleErrorAlertClick = () => {
        setAlert({ content: 'There was an error!', timeout: 2000, negative: true, header: 'Uh Oh' })
    }
    const handleSuccessAlertClick = () => {
        setAlert({ content: 'Something good happend!', timeout: 2000, positive: true, header: 'Awesome!' })
    }
    const handleWarningAlertClick = () => {
        setAlert({ content: 'Warning: here be dragons.', timeout: 2000, warning: true, header: 'Tread Carefully' })
    }
    const handleInfoAlertClick = () => {
        setAlert({
            content: 'FYI... here is some info',
            timeout: 2000,
            info: true,
            header: 'Information: follows',
        })
    }

    return (
        <div>
            <Button negative onClick={handleErrorAlertClick} content="Error Alert" />
            <Button positive onClick={handleSuccessAlertClick} content="Success Alert" />
            <Button color="yellow" onClick={handleWarningAlertClick} content="Warning Alert" />
            <Button primary onClick={handleInfoAlertClick} content="Info Alert" />
        </div>
    )
}

export default HomePage
