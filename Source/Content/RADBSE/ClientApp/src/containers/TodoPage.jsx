import React, { useEffect, useState, useCallback } from 'react'
import { Header, Form, Button, Segment, List } from 'semantic-ui-react'
import { useSession } from '../api/session'
import { useDocumentTitle, useForm } from '../hooks'
import * as Yup from 'yup'
import * as styles from './TodoPage.module.css'
import ErrorMessage from '../components/ErrorMessage'
import { useGetTodos, useAddTodo, useTodos, useUpdateTodo } from '../api/todo'

const TodoPage = () => {
    useDocumentTitle('RADBSE | Todo')
    const [loading, todos] = useTodos()
    const [addTodoloading, addTodo] = useAddTodo()
    const [updateTodoloading, updateTodo] = useUpdateTodo()

    const [error, setError] = useState(null)

    const validation = Yup.object({
        todo: Yup.string().required('Required'),
    })

    const [fields, form] = useForm({
        initialValues: [{ name: 'todo', value: '', label: 'Todo', type: 'input' }],
        validationSchema: validation,
        validateOnBlur: false,
        onSubmit: async ({ todo }) => {
            setError(null)
            try {
                await addTodo({ name: todo })
                form.resetForm()
            } catch (error) {
                console.log(error)
                setError(error)
            }
        },
    })

    const toggleComplete = async (e, { id }) => {
        const todo = todos.find((t) => t.id === id)
        if (todo) {
            try {
                await updateTodo({ ...todo, isCompleted: !todo.isCompleted })
            } catch (error) {
                setError(error)
            }
        }
    }

    return (
        <div className={styles.wrapper}>
            <Form error onSubmit={form.handleSubmit}>
                <ErrorMessage error={error} />
                <div className={styles.todo}>
                    <Form.Input className={styles.stretch} {...fields.todo} />
                    <Form.Field>
                        <Button style={{ marginTop: 24, marginLeft: 10 }} icon="plus" type="submit" />
                    </Form.Field>
                </div>
            </Form>

            <Segment>
                <List>
                    {todos.map((todo) => (
                        <List.Item id={todo.id} onClick={toggleComplete}>
                            {!todo.isCompleted && (
                                <List.Icon name="check circle outline" className={styles.hoverIcon} size="large" />
                            )}
                            {todo.isCompleted && <List.Icon name="check circle" color="green" size="large" />}
                            <List.Content>
                                <List.Header>{todo.name}</List.Header>
                            </List.Content>
                        </List.Item>
                    ))}
                </List>
            </Segment>
        </div>
    )
}

export default TodoPage
