import React from 'react'
import { Route } from 'react-router-dom'
import HomepageHeading from '../components/HomepageHeading'
import ResponsiveNavbar from '../components/ResponsiveNavbar'
import HomePage from './HomePage'
import Alert from '../components/Alert'

const Layout = () => {
    return (
        <div>
            <Alert />
            <ResponsiveNavbar>
                <HomepageHeading path="/" />
            </ResponsiveNavbar>
            <Route path="/" component={HomePage} />
        </div>
    )
}

export default Layout
