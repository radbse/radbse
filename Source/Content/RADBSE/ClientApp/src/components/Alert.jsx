import React, { useEffect, useState } from 'react'
import { useRecoilState } from 'recoil'
import { Portal, Message } from 'semantic-ui-react'
import { alertState } from '../api/app'

const Alert = () => {
    const [alert, setAlert] = useRecoilState(alertState)
    const [messageTimer, setMessageTimer] = useState(null)

    useEffect(() => {
        if (alert.content && alert.timeout) {
            setMessageTimer(
                setTimeout(() => {
                    setMessageTimer(null)
                    setAlert({ content: null, timeout: 0 })
                }, alert.timeout)
            )
        }
    }, [alert, setAlert])

    const handleClose = () => {
        setAlert({ content: null, timeout: 0 })
        if (messageTimer) {
            clearTimeout(messageTimer)
            setMessageTimer(null)
        }
    }

    return (
        <Portal closeOnDocumentClick open={!!alert.content} onClose={handleClose}>
            <div
                style={{
                    position: 'fixed',
                    top: '0',
                    zIndex: 1000,
                    display: 'flex',
                    width: '100%',
                    margin: '20px',
                    justifyContent: 'center',
                }}
            >
                {alert.content ? <Message {...alert} /> : alert.content}
            </div>
        </Portal>
    )
}

export default Alert
