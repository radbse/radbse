import React, { useState, useCallback } from 'react'
// import { navigate } from '@reach/router'
import { Sidebar, Menu, Segment, Container, Button, Icon } from 'semantic-ui-react'
import { useSession, useLogout, useLogin, useRegister } from '../../api/session'
import { useMediaQuery } from '../../hooks/useMediaQuery'

const MobileNavbar = ({ children }) => {
    const logout = useLogout()
    const login = useLogin()
    const register = useRegister()
    const session = useSession()
    const isMobile = useMediaQuery('(max-width: 768px)')
    const [sidebarOpened, setSidebarOpened] = useState(false)
    const handleSidebarHide = useCallback(() => setSidebarOpened(false), [])
    const handleToggle = useCallback(() => setSidebarOpened(!sidebarOpened), [sidebarOpened])
    const handleClick = () => {}
    // const handleLogin = useCallback(() => navigate('/login'), [])
    // const handleSignup = useCallback(() => navigate('/signup'), [])
    // const handleLogout = useCallback(() => navigate('/logout'), [])

    return (
        <>
            {isMobile && (
                <>
                    <Sidebar
                        as={Menu}
                        animation="push"
                        inverted
                        onHide={handleSidebarHide}
                        vertical
                        visible={sidebarOpened}
                    >
                        <Menu.Item as="a" active>
                            Home
                        </Menu.Item>
                        <Menu.Item as="a">Work</Menu.Item>
                        <Menu.Item as="a">Company</Menu.Item>
                        <Menu.Item as="a">Careers</Menu.Item>

                        {!session.isAuthenticated && (
                            <Menu.Item as="a" onClick={() => login('/')}>
                                Log in
                            </Menu.Item>
                        )}
                        {!session.isAuthenticated && (
                            <Menu.Item as="a" onClick={register}>
                                Sign Up
                            </Menu.Item>
                        )}
                        {session.isAuthenticated && (
                            <Menu.Item as="a" onClick={() => logout('/')}>
                                Log out
                            </Menu.Item>
                        )}
                    </Sidebar>
                    <Sidebar.Pusher dimmed={sidebarOpened}>
                        <Segment
                            inverted
                            textAlign="center"
                            style={{
                                minHeight: React.Children.count(children) ? 350 : 0,
                                padding: '1em 0em',
                            }}
                            vertical
                        >
                            <Container>
                                <Menu inverted pointing secondary size="large">
                                    <Menu.Item onClick={handleToggle}>
                                        <Icon name="sidebar" />
                                    </Menu.Item>
                                    <Menu.Item position="right">
                                        {!session.isAuthenticated && (
                                            <Button as="a" inverted onClick={() => login('/')}>
                                                Log in
                                            </Button>
                                        )}
                                        {session.isAuthenticated && (
                                            <Button as="a" inverted onClick={() => logout('/')}>
                                                Log out
                                            </Button>
                                        )}
                                        {!session.isAuthenticated && (
                                            <Button as="a" inverted onClick={register} style={{ marginLeft: '0.5em' }}>
                                                Sign Up
                                            </Button>
                                        )}
                                    </Menu.Item>
                                </Menu>
                            </Container>
                            {children}
                        </Segment>
                    </Sidebar.Pusher>
                </>
            )}
        </>
    )
}

export default MobileNavbar
