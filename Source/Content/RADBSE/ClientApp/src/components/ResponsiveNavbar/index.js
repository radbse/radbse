import React from 'react'
import DesktopNavbar from './DesktopNavbar'
import MobileNavbar from './MobileNavbar'

const ResponsiveNavbar = ({ children }) => (
    <div>
        <DesktopNavbar>{children}</DesktopNavbar>
        <MobileNavbar>{children}</MobileNavbar>
    </div>
)

export default ResponsiveNavbar
