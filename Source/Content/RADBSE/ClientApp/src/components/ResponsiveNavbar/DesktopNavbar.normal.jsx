import React, { useState, useCallback, useContext } from 'react'
import { useLocation, useHistory } from 'react-router'
import { Visibility, Menu, Segment, Container, Button } from 'semantic-ui-react'
import { useSession, useLogout, useLogin, useRegister } from '../../api/session'
import { useMediaQuery } from '../../hooks/useMediaQuery'

const DesktopNavbar = ({ children }) => {
    const logout = useLogout()
    const login = useLogin()
    const register = useRegister()
    const session = useSession()
    const history = useHistory()
    const isDesktop = useMediaQuery('(min-width: 768px)')
    const [fixed, setFixed] = useState(false)
    const hideFixedMenu = useCallback(() => setFixed(false), [])
    const showFixedMenu = useCallback(() => setFixed(true), [])
    const handleClick = (e, { path }) => history.push(path)

    const location = useLocation()

    return (
        <>
            {isDesktop && (
                <Visibility once={false} onBottomPassed={showFixedMenu} onBottomPassedReverse={hideFixedMenu}>
                    <Segment
                        inverted
                        textAlign="center"
                        style={{
                            minHeight: location.path === '/' ? 700 : 0,
                            padding: '1em 0em',
                        }}
                        vertical
                    >
                        <Menu
                            fixed={fixed ? 'top' : null}
                            inverted={!fixed}
                            pointing={!fixed}
                            secondary={!fixed}
                            size="large"
                        >
                            <Container>
                                <Menu.Item as="a" active={location.path === '/'} onClick={handleClick} path="/">
                                    Home
                                </Menu.Item>

                                <Menu.Item position="right">
                                    {!session.isAuthenticated && (
                                        <Button as="a" inverted={!fixed} onClick={() => login('/')}>
                                            Log in
                                        </Button>
                                    )}
                                    {session.isAuthenticated && (
                                        <Button as="a" inverted={!fixed} onClick={() => logout('/')}>
                                            Log out
                                        </Button>
                                    )}
                                    {!session.isAuthenticated && (
                                        <Button
                                            as="a"
                                            inverted={!fixed}
                                            primary={fixed}
                                            onClick={register}
                                            style={{ marginLeft: '0.5em' }}
                                        >
                                            Sign Up
                                        </Button>
                                    )}
                                </Menu.Item>
                            </Container>
                        </Menu>
                        {children}
                    </Segment>
                </Visibility>
            )}
        </>
    )
}

export default DesktopNavbar
