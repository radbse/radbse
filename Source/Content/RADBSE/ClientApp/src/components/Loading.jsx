import React from 'react'
import { Loader } from 'semantic-ui-react'
const Loading = ({ content }) => {
    return <Loader active content={content} size="huge" />
}

export default Loading
