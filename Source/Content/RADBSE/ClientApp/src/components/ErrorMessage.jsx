import React from 'react'
import { Message } from 'semantic-ui-react'

const ErrorMessage = ({ error }) => {
    return (
        <>
            {error && (
                <Message
                    error
                    header={error.message}
                    content={!Array.isArray(error.error) ? error.error : null}
                    list={Array.isArray(error.error) ? error.error : null}
                />
            )}
        </>
    )
}

export default ErrorMessage
