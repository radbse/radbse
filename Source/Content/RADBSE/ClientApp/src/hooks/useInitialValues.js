export const useInitialValues = (initialValues) => {
    if (!Array.isArray(initialValues)) {
        return [
            initialValues,
            Object.keys(initialValues).reduce((fields, field) => {
                fields[field] = { name: field, value: initialValues[field] }
                return fields
            }, {}),
        ]
    }

    return [
        initialValues.reduce((fields, field) => {
            fields[field.name] = field.value
            return fields
        }, {}),
        initialValues.reduce((fields, field) => {
            fields[field.name] = field
            return fields
        }, {}),
    ]
}
