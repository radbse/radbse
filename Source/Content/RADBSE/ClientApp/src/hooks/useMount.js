import { useEffect } from 'react'
export const useMount = (effect) => {
    // eslint-disable-next-line react-hooks/exhaustive-deps
    useEffect(effect, [])
}
