import md5 from 'md5'
import { useMemo } from 'react'

export const useGravatar = (email, size) => {
    return useMemo(() => `https://secure.gravatar.com/avatar/${md5(email.toLowerCase().trim())}?d=mp&s=${size}`, [
        email,
        size,
    ])
}
