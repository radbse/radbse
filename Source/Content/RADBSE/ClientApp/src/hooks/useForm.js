import { useFormik } from 'formik'
import { useInitialValues } from './useInitialValues'

export const useForm = ({ initialValues: incomingValues, ...rest }) => {
    const [initialValues, fields] = useInitialValues(incomingValues)
    const form = useFormik({ initialValues, ...rest })
    Object.keys(fields).forEach((field) => {
        const opts = {}
        const f = fields[field]
        if (f.type === 'checkbox') {
            const props = form.getFieldProps(field)
            opts.onChange = (e, { checked }) => form.setFieldValue(f.name, checked)
            opts.checked = props.value
        }
        if (f.type === 'dropdown') {
            const props = form.getFieldProps(field)
            opts.onChange = (e, { value }) => form.setFieldValue(f.name, value)
            opts.value = props.value
        }

        fields[field] = {
            ...fields[field],
            ...form.getFieldProps(field),
            error: form.touched[field] ? form.errors[field] : null,
            ...opts,
        }
    })

    form.arrayMap = (field, callBack) => {
        const getField = (item, idx) => {
            return (f) => {
                const fieldName = `${field.name}.${idx}.${f}`
                let error = null
                if (form.errors[field.name] && form.errors[field.name][idx] && form.errors[field.name][idx][f])
                    error = form.errors[field.name][idx][f]
                return {
                    name: fieldName,
                    value: item[f],
                    error: error,
                    onChange: (e, { value }) => form.setFieldValue(fieldName, value),
                }
            }
        }
        return field.value?.map((item, index) => {
            return callBack(getField(item, index), index)
        })

        return null
    }
    return [fields, form]
}
