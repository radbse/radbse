using Microsoft.AspNetCore.Authentication;
using Microsoft.EntityFrameworkCore;
#if (!NoDb)
using RADBSE.Data;
#endif
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.IO;
using System.Reflection;
using System;
using System.Linq;

namespace RADBSE.Utils
{
    public class Utility
    {
        public static void Run(string[] args)
        {
            var serviceColleciton = new ServiceCollection();
            ConfigureServices(serviceColleciton);

            var serviceProvider = serviceColleciton.BuildServiceProvider();
            var app = new Microsoft.Extensions.CommandLineUtils.CommandLineApplication();
            var program = serviceProvider.GetService<Utility>();

            var version = typeof(Utility).GetTypeInfo().Assembly.GetName().Version;
            var name = typeof(Utility).GetTypeInfo().Assembly.GetName().Name;

            Console.WriteLine($"{name} - {version}");

#if (!NoDb)
            app.Command("database", config =>
            {
                config.OnExecute(() =>
                {
                    program.MigrateDb();
                    return 0;
                });
            });
#endif
            app.HelpOption("-? | -h | --help");
            app.OnExecute(() =>
            {
                app.ShowHelp();
                return 0;
            });
            app.Execute(args);
        }

#if (!NoDb)
        private readonly ApplicationDbContext _dbContext;

        public Utility(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }
#endif

#if (!NoDb)
        public void MigrateDb()
        {
            Console.WriteLine();
            Console.WriteLine($"Database: {_dbContext.Database.GetDbConnection().Database}");
            Console.WriteLine();
            Console.WriteLine("Found Migrations");
            _dbContext.Database.GetMigrations().ToList().ForEach(migration => Console.WriteLine(migration));

            Console.WriteLine();
            Console.WriteLine("Applied Migrations");
            _dbContext.Database.GetAppliedMigrations().ToList().ForEach(migration => Console.WriteLine(migration));

            Console.WriteLine();
            Console.WriteLine("Pending Migrations");
            var pending = _dbContext.Database.GetPendingMigrations();
            pending.ToList().ForEach(migration => Console.WriteLine(migration));

            if (pending.Any())
            {
                Console.WriteLine();
                Console.WriteLine("applying migrations...");
                try
                {
                    _dbContext.Database.Migrate();
                    Console.WriteLine("done...");
                }
                catch (System.Exception)
                {
                    Console.Error.WriteLine("Could not apply migrations");
                    throw;
                }

            }
            else
            {
                Console.WriteLine();
                Console.WriteLine("NOTHING TO DO!");
            }
        }
#endif

#if (NoDb)

        public Utility()
        {
        }
#endif
        private static void ConfigureServices(IServiceCollection services)
        {
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddUserSecrets<Utility>()
                .AddEnvironmentVariables()
                .Build();

            services.AddSingleton(configuration);
#if (!NoDb || !NoAuth)
            services.AddDbContext<ApplicationDbContext>(options =>
#if (IsSqlite)
                options.UseSqlite(
#endif
#if (IsSqlServer)
                options.UseSqlServer(
#endif
#if (IsNpgSql)
                options.UseNpgsql(
                    configuration.GetConnectionString("DefaultConnection")).UseSnakeCaseNamingConvention());
#endif
#if (!IsNpgSql)
                    configuration.GetConnectionString("DefaultConnection")));
#endif

            services.AddDatabaseDeveloperPageExceptionFilter();
#endif

#if (!NoAuth)
            services.AddDefaultIdentity<ApplicationUser>(options => options.SignIn.RequireConfirmedAccount = true)
                .AddEntityFrameworkStores<ApplicationDbContext>();

            services.AddIdentityServer()
                .AddApiAuthorization<ApplicationUser, ApplicationDbContext>();

            services.AddAuthentication()
                .AddIdentityServerJwt();
#endif
            services.AddTransient<Utility>();
        }
    }
}