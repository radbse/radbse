# Initial Dev Environment Setup

## Clone Repo

After cloning the repo

1. ```
   > dotnet restore
   ```
2. ```
   > dotnet build
   ```

## Docker Commands

## Add Secrets

### Setup connection string

```
dotnet user-secrets set "ConnectionStrings:DefaultConnection" "DataSource=app.db;Cache=Shared" --project RADBSE/RADBSE.csproj
```

## Run migrations

```
dotnet ef database update --project RADBSE/RADBSE.csproj
```

or

```
dotnet run database --project RADBSE/RADBSE.csproj
```

## Additional NPM commands

### Add Migration

To add a migration add a POCO object to RADBSE.Data and Add a corresponding DbSet<> to RADBSE.Data.ApplicationDbContext then

```
dotnet ef migrations add MyMigrationName --project RADBSE/RADBSE.csproj
```

# Build and Run

## Build Docker Container

> docker build -t radbseapp .

## Run container for the 1st time

> mkdir ~/.radbsedata && cd ~/.radbsedata

> openssl genrsa 2048 > private.pem

> openssl req -x509 -new -key private.pem -out public.pem

> openssl pkcs12 -export -in public.pem -inkey private.pem -out cert.pfx

> docker run -d -p 8080:80 -v ~/.radbsedata:/app/data -e IdentityServer:Key:Password=certpassword --name radbse radbseapp
