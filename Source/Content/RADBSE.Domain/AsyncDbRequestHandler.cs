using System.Threading;
using System.Threading.Tasks;
using System.Transactions;
using RADBSE.Data;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Linq;
using System.Security.Claims;

namespace RADBSE.Domain
{
    public abstract class AsyncDbRequestHandler<TRequest, TResponse> : IRequestHandler<TRequest, TResponse> where TRequest : IRequest<TResponse>
    {
        private readonly HttpContext _context;

        protected AsyncDbRequestHandler(ApplicationDbContext dbContext, IHttpContextAccessor httpContextAccessor)
        {
            DbContext = dbContext;
            _context = httpContextAccessor.HttpContext;
        }

        public ApplicationDbContext DbContext { get; }
        public string UserId
        {
            get
            {
                if (_context?.User == null)
                    return Guid.Empty.ToString();

                var id = _context.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier);
                if (id == null)
                    return Guid.Empty.ToString();

                return new Guid(id.Value).ToString();
            }
        }

        public TransactionScope CreateRequiredScope()
        {
            return new TransactionScope(TransactionScopeOption.Required, TransactionScopeAsyncFlowOption.Enabled);
        }

        public TransactionScope CreateReadUncommitted()
        {
            return new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.ReadUncommitted }, TransactionScopeAsyncFlowOption.Enabled);
        }

        protected abstract Task<TResponse> Handle(TRequest message);
        public Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken) => Handle(request);
    }

    public abstract class AsyncDbRequestHandler<TRequest> : AsyncRequestHandler<TRequest> where TRequest : IRequest
    {
        private readonly HttpContext _context;

        protected AsyncDbRequestHandler(ApplicationDbContext dbContext, IHttpContextAccessor httpContextAccessor)
        {
            DbContext = dbContext;
            _context = httpContextAccessor.HttpContext;
        }

        public ApplicationDbContext DbContext { get; }
        public string UserId
        {
            get
            {
                if (_context?.User == null)
                    return Guid.Empty.ToString();

                var id = _context.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier);
                if (id == null)
                    return Guid.Empty.ToString();

                return new Guid(id.Value).ToString();
            }
        }

        public TransactionScope CreateRequiredScope()
        {
            return new TransactionScope(TransactionScopeOption.Required, TransactionScopeAsyncFlowOption.Enabled);
        }

        public TransactionScope CreateReadUncommitted()
        {
            return new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.ReadUncommitted }, TransactionScopeAsyncFlowOption.Enabled);
        }

        protected abstract Task Handle(TRequest message);
        protected override Task Handle(TRequest message, CancellationToken cancellationToken) => Handle(message);
    }
}