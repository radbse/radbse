using System;
using FluentValidation;
using MediatR;
using Mapster;
using RADBSE.Data;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace RADBSE.Domain
{
    public class CreateTodo
    {
        public class Command : IRequest
        {
            public Guid Id { get; set; } = Guid.NewGuid();
            public string Name { get; set; }
        }

        public class Validator : AbstractValidator<Command>
        {
            public Validator()
            {
                RuleFor(m => m.Id).NotEmpty();
                RuleFor(m => m.Name).NotEmpty();
            }
        }

        public class Handler : AsyncDbRequestHandler<Command>
        {
            public Handler(ApplicationDbContext context, IHttpContextAccessor httpContextAccessor) : base(context, httpContextAccessor)
            {

            }

            protected async override Task Handle(Command message)
            {
                var todo = message.Adapt<Todo>();
                todo.UserId = UserId;
                await DbContext.AddAsync(todo).ConfigureAwait(false);
                await DbContext.SaveChangesAsync().ConfigureAwait(false);
            }
        }
    }
}