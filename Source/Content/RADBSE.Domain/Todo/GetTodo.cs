using System;
using System.Threading.Tasks;
using MediatR;
using Mapster;
using RADBSE.Data;
using FluentValidation;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;

namespace RADBSE.Domain
{
    public class GetTodo
    {
        public class Query : IRequest<Model>
        {
            public Guid Id { get; set; }

        }

        public class Model
        {
            public Guid Id { get; set; }
            public string Name { get; set; }
            public bool IsCompleted { get; set; }
        }

        public class Validator : AbstractValidator<Query>
        {
            public Validator()
            {
                RuleFor(m => m.Id).NotEmpty();
            }
        }

        public class Handler : AsyncDbRequestHandler<Query, Model>
        {
            public Handler(ApplicationDbContext context, IHttpContextAccessor httpContextAccessor) : base(context, httpContextAccessor)
            {

            }

            protected async override Task<Model> Handle(Query message)
            {
                var todo = await DbContext.Todos.FindAsync(message.Id).ConfigureAwait(false);
                return todo.Adapt<Model>();
            }
        }
    }
}