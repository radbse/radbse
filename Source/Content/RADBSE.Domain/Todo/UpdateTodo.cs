using System;
using FluentValidation;
using MediatR;
using Mapster;
using RADBSE.Data;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace RADBSE.Domain
{
    public class UpdateTodo
    {
        public class Command : IRequest
        {
            public Guid Id { get; set; }
            public string Name { get; set; }
            public bool IsCompleted { get; set; }
        }

        public class Validator : AbstractValidator<Command>
        {
            public Validator()
            {
                RuleFor(m => m.Id).NotEmpty();
                RuleFor(m => m.Name).NotEmpty();
            }
        }

        public class Handler : AsyncDbRequestHandler<Command>
        {
            public Handler(ApplicationDbContext context, IHttpContextAccessor httpContextAccessor) : base(context, httpContextAccessor)
            {

            }

            protected async override Task Handle(Command message)
            {
                var todo = await DbContext.Todos.FindAsync(message.Id).ConfigureAwait(false);
                message.Adapt(todo);
                await DbContext.SaveChangesAsync().ConfigureAwait(false);
            }
        }
    }
}