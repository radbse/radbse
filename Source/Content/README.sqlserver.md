# Initial Dev Environment Setup

## Clone Repo

After cloning the repo

1. ```
   > dotnet restore
   ```
2. ```
   > dotnet build
   ```

## Docker Commands

### Database Setup (SqlServer)

If you need to install sql server on your dev box, use docker, its this easy.

```
docker run --name radbse_sql -e 'ACCEPT_EULA=Y' -e 'SA_PASSWORD=yourStrong(!)Password' -p 1433:1433 -d mcr.microsoft.com/mssql/server:2017-CU8-ubuntu
```

## Add Secrets

### Setup connection string

```
dotnet user-secrets set ConnectionStrings:DefaultConnection 'Data Source=localhost;Initial Catalog=RADBSE;Integrated Security=false;User ID=sa;Password=yourStrong(!)Password;' --project RADBSE/RADBSE.csproj
```

## Run migrations

```
dotnet ef database update --project RADBSE/RADBSE.csproj
```

or

```
dotnet run database --project RADBSE/RADBSE.csproj
```

## Additional NPM commands

### Add Migration

To add a migration add a POCO object to RADBSE.Data and Add a corresponding DbSet<> to RADBSE.Data.ApplicationDbContext then

```
dotnet ef migrations add MyMigrationName --project RADBSE/RADBSE.csproj
```

# Build and Run

## Build Docker Container

> docker build -t radbseapp .

## Run container for the 1st time

> mkdir ~/.radbsedata && cd ~/.radbsedata

> openssl genrsa 2048 > private.pem

> openssl req -x509 -new -key private.pem -out public.pem

> openssl pkcs12 -export -in public.pem -inkey private.pem -out cert.pfx

> docker run -d -p 8080:80 -v ~/.radbsedata:/app/data -e IdentityServer:Key:Password=certpassword --name radbse radbseapp
