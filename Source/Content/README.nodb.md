# Initial Dev Environment Setup

## Clone Repo

After cloning the repo

1. ```
   > dotnet restore
   ```
2. ```
   > dotnet build
   ```

# Build and Run

## Build Docker Container

> docker build -t radbseapp .

## Run container for the 1st time

> mkdir ~/.radbsedata && cd ~/.radbsedata

> openssl genrsa 2048 > private.pem

> openssl req -x509 -new -key private.pem -out public.pem

> openssl pkcs12 -export -in public.pem -inkey private.pem -out cert.pfx

> docker run -d -p 8080:80 -v ~/.radbsedata:/app/data -e IdentityServer:Key:Password=certpassword --name radbse radbseapp
